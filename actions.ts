
import { ILoginResponse, LoginGetAction } from '../../../types';
import { LOGIN_GET } from './actionsTypes';

export const loginAuth = (userInfo: ILoginResponse): LoginGetAction => ({
  type: LOGIN_GET,
  userInfo
});
