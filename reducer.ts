
import { LoginGetAction, LoginState } from '../../../types';
import { LOGIN_GET } from './actionsTypes';

const initialState: LoginState = {
  userStatus: {
    status: '',
    message: ''
  }
};

const loginReducer = (
  state: LoginState = initialState,
  action: LoginGetAction
): LoginState => {
  switch (action.type) {
    case LOGIN_GET:
      return { ...state, ...action.userInfo };
    default:
      return state;
  }
};

export default loginReducer;
