import { createStore, combineReducers, applyMiddleware, Store } from 'redux';
import thunk from 'redux-thunk';
import loginReducer from './modules/login/reducer';

const reducer = combineReducers({
  login: loginReducer
});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;
